<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        <!--AngularJS-->
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.12/angular.min.js"></script>
        <script src="js/app.js"></script>
    </head>
    <body>
        <!-- <div class="loader"></div> -->
        @yield('content')
    </body>
</html>